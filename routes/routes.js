const router = require('express').Router();
const bodyParser = require('body-parser');
const namesChecker = require('../controllers/checker').check_string;
const namesChecker2 = require('../controllers/checker').check_many_strings;
const saveBadUser =  require('../controllers/savers/insertBLuser').run;
// Check is any client credentials match one string name
router.post('/exist1/', bodyParser.json(), async (req, res) => {
    console.log('req.params.name: ', req.params.name);
    if(req.body.hasOwnProperty('string')) namesChecker(req.body.string, res);
    else res.send({error: "No \'string\' property"});
});

// Check is any client credentials match two string names
router.post('/exist2/', bodyParser.json(), async (req, res) => {
    console.log('string1: ', req.body.string1);
    console.log('string2: ', req.body.string2);
    namesChecker2(req.body.string1, req.body.string2, res);
});



// insert new black user
//router.post(),w

router.post('/save/', bodyParser.json(), async (req, res) => {
    console.log('request to insert: \n', req.body);
    saveBadUser(req.body, res);
});

module.exports = router;