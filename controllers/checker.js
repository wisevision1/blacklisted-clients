const firstNamesCheck   = require('./checkers/checkFirstNames').run;
const lastNamesCheck    = require('./checkers/checkLastNames').run;
const secondNamesCheck  = require('./checkers/checkSecondNames').run;

const namesCheck1  = require('./checkers/checkNames').one_str;
const namesCheck2  = require('./checkers/checkNames').two_str;



exports.check_string = async (to_check, res) => {
    
    const result   = await namesCheck1(`${to_check}`);
    console.log('result:\t',  result);        
    res.send({ result: result });
};

exports.check_many_strings = async (str1, str2, res) => {

    const result   = await namesCheck2(`${str1}`, `${str2}`);
    console.log('result:\t',  result);        
    res.send({ result: result });
};

