const  executeQuery = require('../../models/mysql').executeQuery;
const  getAll_first_names = require('../../models/queries').getAll_first_names;
//const  getAll_by_ids = require('../../models/queries').getAll_by_ids;

exports.run = async (to_check) => {
    console.log('checking: \t', to_check);
    
    // 0. Validate string
    if(typeof to_check !== "string") { console.log('not a string'); return; } 
    if(to_check.length < 1) { console.log('not a string'); return; } 

    const query = await getAll_first_names(to_check);
    const result = await executeQuery(query);
    // console.log(result);
    return result;
};

/*
exports.run = async (to_check) => {
    const startTime = Date.now();
    // 0. Validate string
    if(typeof to_check !== "string") { console.log('not a string'); return; } 
    if(to_check.length < 1) { console.log('not a string'); return; } 

    console.log(await this.logic(to_check));

    console.log('milisecons',Date.now() - startTime);
};
this.logic = async (to_check) => {
    const map = await this.getMap();
    const users_ids = [];
    for(const [key, value] of map.entries()) {
        //console.log(14, value);
        if(value.includes(to_check)) users_ids.push(key);
    };
    //console.log(18, users_ids);
    // get users by ids
    const query2 = await getAll_by_ids(users_ids);
    const result = await executeQuery(query2);
    return result;
    
};

this.getMap = async () => {
    const query = await getAll_first_names();
    const result = await executeQuery(query);
    // console.log(6, result);
    let resultMap;
    if(Array.isArray(result) && result.length > 0) {
        resultMap = await this.arrayToMap(result);
        //console.log(25, resultMap);
        return resultMap;
    }
    else {
        // throw 'Query error'
        return Error('Query error');
    }
};

this.arrayToMap = async (arr) => {
    const map = new Map();
    for(const obj of arr) map.set(obj.id, obj.first_name);
    return map;
};
*/

// this.run('st2');