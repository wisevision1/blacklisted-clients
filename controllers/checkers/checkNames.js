const  executeQuery = require('../../models/mysql').executeQuery;
const  getAll_names = require('../../models/queries').getAll_names;
const  getAll_names_2 = require('../../models/queries').getAll_names_2;

exports.one_str = async (to_check) => {
    // 0. Validate string
    if(typeof to_check !== "string") { console.log('not a string'); return; } 
    if(to_check.length < 1) { console.log('not a string'); return; } 

    const query = await getAll_names(to_check);
    const result = await executeQuery(query);
    // console.log(result);
    return result;
};

exports.two_str = async (str1, str2) => {
    // 0. Validate string
    if(typeof str1 !== "string") { console.log('not a string'); return; } 
    if(typeof str2 !== "string") { console.log('not a string'); return; } 
    if(str1.length < 1) { console.log('not a string'); return; } 
    if(str2.length < 1) { console.log('not a string'); return; } 

    const query = await getAll_names_2(str1, str2);
    const result = await executeQuery(query);
    // console.log(result);
    return result;
};