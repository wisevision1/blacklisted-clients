const  executeQuery = require('../../models/mysql').executeQuery;
const  getAll_second_names = require('../../models/queries').getAll_second_names;


exports.run = async (to_check) => {
    // 0. Validate string
    if(typeof to_check !== "string") { console.log('not a string'); return; } 
    if(to_check.length < 1) { console.log('not a string'); return; } 

    const query = await getAll_second_names(to_check);
    const result = await executeQuery(query);
    // console.log(result);
    return result;
};

// this.run('st2');