const  executeQuery = require('../../models/mysql').executeQuery;
const  checkAllFields = require('../../models/queries').checkAllFields;
const  insert = require('../../models/queries').insert;

exports.run = async (values, res) => {
    // 0. Validate values
    const errors = [];
    if(
        (values.first_name   === undefined || values.first_name   === null || values.first_name   === '') &&
        (values.last_name    === undefined || values.last_name    === null || values.last_name    === '') &&
        (values.second_name  === undefined || values.second_name  === null || values.second_name  === '')
    ) errors.push('One of first, second, last names must be specified! At least two woud be better ');
    

    if(errors.length > 0) {
        res.send({errors: errors});
        return;
    };
    // 1. Check existance
    const mock = {
        first_name:     await emptyNullReturn(values.first_name),
        last_name:      await emptyNullReturn(values.last_name),
        second_name:    await emptyNullReturn(values.second_name),
        phone:          await emptyNullReturn(values.phone),
        email:          await emptyNullReturn(values.email),
        in_hotel:       await emptyNullReturn(values.in_hotel),
        comment:        await emptyNullReturn(values.comment)
    };
    
    //console.log(mock);
    
    const query1 = await checkAllFields(mock);
    const result1 = await executeQuery(query1);

    //console.log(result1);
    
    if(result1.length > 0) {
        res.send({
            result: "Exactly the same already exists",
            user: result1
        });
        return;
    }

    // 2. If not exists and is ok paste it to db

    const query = await insert(to_check);
    const result = await executeQuery(query);

    res.send({result: "User saved"})
};

const emptyNullReturn = async (value) => {
    if(value === undefined || value === null || value === '') return null;
    else return value;
};
//this.run('str');