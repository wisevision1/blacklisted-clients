var forever = require('forever-monitor');

var child = new (forever.Monitor)('app.js', {
  max: 2,
  silent: false,
  args: []
});

child.on('exit', function () {
  console.log('app.js has exited after 2 restarts');
});

child.start();