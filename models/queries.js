exports.getAllrecords = async () => {
    const query = `
    SELECT * FROM blacklist_schema.blacklisted_users;
    `;
    return query;
};

// test: js way
/*
exports.getAll_first_names = async () => {
    const query = `
    SELECT id, first_name FROM blacklist_schema.blacklisted_users;
    `;
    return query;
};

exports.getAll_by_ids = async (ids) => {
    let str = '';
    for(const id of ids) str += `\n\t id = ${id} OR`;
    const query = `
    SELECT * FROM blacklist_schema.blacklisted_users
    WHERE ${str.substring(0, str.length - 2)};
    `;
    //console.log(query);
    
    return query;
};
*/



// test: sql way 1
exports.getAll_first_names = async (match) => {
    const query = `
    SELECT * FROM blacklist_schema.blacklisted_users
    WHERE first_name LIKE '%${match}%';
    `;
    console.log(query);
    
    return query;
};


exports.getAll_last_names = async (match) => {
    const query = `
    SELECT * FROM blacklist_schema.blacklisted_users
    WHERE last_name LIKE '%${match}%';
    `;
    console.log(query);
    return query;
};

exports.getAll_second_names = async (match) => {
    const query = `
    SELECT * FROM blacklist_schema.blacklisted_users
    WHERE second_name LIKE '%${match}%';
    `;
    console.log(query);
    return query;
};

// test: sql way 2

exports.getAll_names = async (match) => {
    const query = `
    SELECT * FROM blacklist_schema.blacklisted_users
    WHERE
        first_name LIKE '%${match}%' OR 
        last_name LIKE '%${match}%' OR 
        second_name LIKE '%${match}%'
        ;
    `;
    console.log(query);
    return query;
};

exports.getAll_names_2 = async (match1, match2) => {
    const query = `
    SELECT * FROM blacklist_schema.blacklisted_users
    WHERE
        (first_name LIKE '%${match1}%' AND last_name LIKE '%${match2}%' ) OR 
        (first_name LIKE '%${match2}%' AND last_name LIKE '%${match1}%' ) OR 
        (second_name LIKE '%${match1}%' AND last_name LIKE '%${match2}%') OR
        (second_name LIKE '%${match2}%' AND last_name LIKE '%${match1}%') OR
        (first_name LIKE '%${match1}%' AND second_name LIKE '%${match2}%' ) OR 
        (first_name LIKE '%${match2}%' AND second_name LIKE '%${match1}%' ) 
        ;
    `;
    console.log(query);
    return query;
};

// 

exports.checkAllFields = async (mock) => {

    let first_name = '';
    let last_name = '';
    let second_name = '';
    let hotel_imported = '';
    let comment = '';
    let phone = '';
    let email = '';

    if(mock.first_name === null) first_name = 'first_name IS NULL';
    else first_name = `first_name LIKE '${mock.first_name}'`;

    if(mock.last_name === null) last_name = 'last_name IS NULL';
    else last_name = `last_name LIKE '${mock.last_name}'`;

    if(mock.second_name === null) second_name = 'second_name IS NULL';
    else second_name = `second_name LIKE '${mock.second_name}'`;

    if(mock.in_hotel === null) hotel_imported = 'hotel_imported IS NULL';
    else hotel_imported = `hotel_imported LIKE '${mock.in_hotel}'`;

    if(mock.comment === null) comment = 'comment IS NULL';
    else comment = `comment LIKE '${mock.comment}'`;

    if(mock.phone === null) phone = 'phone IS NULL';
    else phone = `phone LIKE '${mock.phone}'`;

    if(mock.email === null) email = 'email IS NULL';
    else email = `email LIKE '${mock.email}'`;



    const query = `
    SELECT * FROM blacklist_schema.blacklisted_users
    WHERE
        (${first_name} ) AND
        (${last_name} ) AND
        (${second_name} ) AND
        (${hotel_imported} ) AND
        (${comment} ) AND
        (${phone} ) AND
        (${email} ) ;
    `;
    console.log(query);
    return query;
};

exports.insert = (values) => {
    const query =`
    INSERT INTO blacklist_schema.blacklisted_users 
    (first_name, last_name, second_name, hotel_imported, comment, phone, email) 
    VALUES 
    ('${values.first_name}', '${values.last_name}', '${values.second_name}', '${values.in_hotel}', '${values.comment}', '${values.phone}', '${values.email}');
    `;
    console.log(query);
    return query;
};