//const mysql = require('mysql2');
const mysql = require('mysql2/promise');
const configs = require('../configs/configs').mysql_connection;

//const connection = await mysql.createConnection(configs);

// connection.query(
//     `
//     SELECT * FROM blacklist_schema.blacklisted_users;
//     `,
//     (err, results, fields) => {
//         if (err) console.log(err);
//         //console.log(results); 
//         const arr = results;
//         console.log(...arr); 
//     });



exports.executeQuery = async (query) => {
    const connection = await mysql.createConnection(configs);
    const [rows, fields] = await connection.execute(query);
    //console.log(rows);
    return rows;
};
