const express = require('express');
const app = express();
const forever = require('forever');

const cors = require('cors');
app.use(cors());

const port = 8000;

const router = express.Router();
const routers = require('./routes/routes');
router.get('/', (req, res) => {
    //res.header("Access-Control-Allow-Origin", "*");
    res.header('Access-Control-Allow-Methods', 'GET, HEAD, CONNECT, TRACE, POST, OPTIONS, PUT, PATCH, DELETE');
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.setHeader('Access-Control-Allow-Credentials', true);
    res.sendFile(path.join(__dirname+'/app/public/index.html'));
     //res.render(path.join(__dirname+'/app/public/index.html'));
});
app.use(routers);

app.listen(port, () => {
    console.log(`Server app UP on ${port}`);
});



/* 

UPDATE mysql.user 
SET authentication_string='root'
WHERE User='root';

*/